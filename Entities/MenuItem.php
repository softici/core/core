<?php

namespace Modules\AdminCore\Entities;

class MenuItem
{

    public $name;

    public $order = 0;

    public $route_name;

    public $icon;

    public $section;


    public function __construct($name, $route_name, $section = null, $order = null, $icon = null)
    {
        $this->name = $name;
        $this->section = $section;
        $this->route_name = $route_name;
        $this->order = $order;
        $this->icon = $icon;
    }
}
