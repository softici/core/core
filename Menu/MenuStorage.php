<?php


namespace Modules\AdminCore\Menu;


use Modules\AdminCore\Entities\MenuItem;

/**
 * Class MenuStorage
 *
 * @package Modules\AdminCore\Menu
 */
class MenuStorage
{
    private $menuItems = [];

    public function addItem(MenuItem $item)
    {
        $this->menuItems[] = $item;
    }

    public function getMenuItems()
    {
        return $this->menuItems;
    }
}
