<?php


namespace Modules\AdminCore\Menu;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Modules\AdminCore\Entities\MenuItem;

/**
 * Class MenuService
 *
 * Admin core auto-loading menu.
 *
 *
 * @package Modules\AdminCore\Services
 */
class MenuService
{
    /**
     * Register new menu item.
     *
     * @param MenuItem $item
     */
    public function registerMenuItem(MenuItem $item)
    {
        $ms = resolve(MenuStorage::class);

        $ms->addItem($item);
    }

    /**
     * Get actual menu data.
     *
     * @return Collection Collection of menu item objects.
     */
    public function getMenuItems()
    {
        $menu_storage = resolve(MenuStorage::class);

        $collection = collect($menu_storage->getMenuItems());

        return $collection;
    }

    /**
     * Return menu grouped by sections.
     *
     * @return Collection
     */
    public function getMenuSections()
    {
        $items = $this->getMenuItems();

        // Hide users CRUD for nonadmin users
        if (@Auth::user()->role != 'admin')
        {
            $items = $items->reject(function (MenuItem $item) {
                return $item->route_name == 'users::users.index';
            });
        }

        return $items->groupBy('section');
    }
}
