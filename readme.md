# Softíci Admin core
Version: 0.1.1


Modular Laravel 7 administration with auto registering menu and simple desktop layout.
Modules are autoloaded using nWidart Laravel modules.

#### Credit
Admincore administration 

## Get started
### Install core module
You can install admincore into your project using Composer:
```
composer config repositories.admincore gitlab https://gitlab.com/softici/core/admincore-module
composer config repositories.users gitlab https://gitlab.com/softici/core/users-module
composer require softici/core-module

php artisan module:enable Admincore
php artisan module:enable Users

php artisan migrate
php artisan module:publish
```

Now you can access admin page on the route `/admin`.
To create default password you can use module seed (`php artisan module:seed`).

Default user: `admin@admin.com`, password: `secret`.

### Install modules
Choose available modules to install using module command:
```
php artisan acore:module
```

### Development
This command will build fresh assets and copy them to your project working directory.
```
yarn
yarn watch
```

