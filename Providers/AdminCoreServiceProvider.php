<?php

namespace Modules\AdminCore\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\AdminCore\Console\Install;
use Modules\AdminCore\Console\InstallModule;
use Modules\AdminCore\Console\SeedUser;
use Modules\AdminCore\Entities\MenuItem;
use Modules\AdminCore\Menu\MenuService;
use Modules\AdminCore\Menu\MenuStorage;

class AdminCoreServiceProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Menu bootstrapping
        $this->registerMenuStorage();
        $this->registerMenuItems();

        $this->registerCommands();

        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path('AdminCore', 'Database/Migrations'));
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }


    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            module_path('AdminCore', 'Config/config.php') => config_path('admincore.php'),
        ], 'config');
        $this->mergeConfigFrom(
            module_path('AdminCore', 'Config/config.php'), 'admincore'
        );
    }


    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/admincore');

        $sourcePath = module_path('AdminCore', 'Resources/views');

        $this->publishes([
            $sourcePath => $viewPath,
        ], 'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path)
        {
            return $path . '/modules/admincore';
        }, \Config::get('view.paths')), [$sourcePath]), 'admincore');
    }


    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/admincore');

        if (is_dir($langPath))
        {
            $this->loadTranslationsFrom($langPath, 'admincore');
        }
        else
        {
            $this->loadTranslationsFrom(module_path('AdminCore', 'Resources/lang'), 'admincore');
        }
    }


    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if ( ! app()->environment('production') && $this->app->runningInConsole())
        {
            app(Factory::class)->load(module_path('AdminCore', 'Database/factories'));
        }
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }


    private function registerMenuStorage()
    {
        //   $this->app->singleton(MenuStorage::class);

        $this->app->singleton(MenuStorage::class, function ($app)
        {
            return new MenuStorage();
        });
    }


    private function registerMenuItems()
    {
        $ms = new MenuService();
        $ms->registerMenuItem((new MenuItem('Úvod', 'admincore::home')));
    }


    private function registerCommands()
    {
        $this->commands([
            InstallModule::class,
            SeedUser::class,
        ]);
    }
}
