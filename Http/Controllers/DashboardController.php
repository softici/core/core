<?php

namespace Modules\AdminCore\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class DashboardController
 *
 * Admin http index.
 *
 * @package Modules\AdminCore\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('admincore::index');
    }
}
