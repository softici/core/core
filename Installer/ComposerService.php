<?php


namespace Modules\AdminCore\Installer;


use Illuminate\Support\Collection;

class ComposerService
{

    public function requirePackage(Collection $modules): bool
    {
        $package_signatures = $modules->implode('package_name', ' ');

        $this->runProcess("composer require --prefer-source $package_signatures");

        return true;
    }


    public function requireRepository($git, $name): bool
    {
        $name = strtolower($name);

        $this->runProcess("composer config repositories.$name gitlab $git");

        return true;
    }


    public function enableGITrepository($name, $git)
    {
        $this->runProcess("cd modules\\$name && git init && git remote origin set-url $git");
    }


    /**
     * @param string $cmd
     *
     * @return void
     */
    private function runProcess(string $cmd)
    {
        passthru($cmd);
    }
}
