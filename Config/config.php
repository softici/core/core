<?php

return [
    'name' => 'AdminCore',

    'modules' => [
        [
            'name'        => 'Borrowings',
            'description' => '',

            'package_name' => 'softici/borrowings-module',
            'git'          => 'https://gitlab.com/softici/admincore/borrowings-module',
        ],

        [
            'name'        => 'Items',
            'description' => '',

            'package_name' => 'softici/items-module',
            'git'          => 'https://gitlab.com/softici/admincore/items-module',
        ],

        [
            'name'        => 'Stock',
            'description' => '',

            'package_name' => 'softici/stock-module',
            'git'          => 'https://gitlab.com/softici/admincore/stock-module',
        ],

        [
            'name'        => 'Orders',
            'description' => '',

            'package_name' => 'softici/orders-module',
            'git'          => 'https://gitlab.com/softici/admincore/orders-module',
        ],

        [
            'name'        => 'Gallery',
            'description' => '',

            'package_name' => 'softici/gallery-module',
            'git'          => 'https://gitlab.com/softici/admincore/gallery-module',
        ],

        [
            'name'        => 'Item-categories',
            'description' => '',

            'package_name' => 'softici/item-categories-module',
            'git'          => 'https://gitlab.com/softici/admincore/item-categories-module',
        ],

        [
            'name'        => 'Captions',
            'description' => '',

            'package_name' => 'softici/captions-module',
            'git'          => 'https://gitlab.com/softici/admincore/captions-module',
        ],

        [
            'name'        => 'Recipients',
            'description' => '',

            'package_name' => 'softici/recipients-module',
            'git'          => 'https://gitlab.com/softici/admincore/recipients-module',
        ],

        [
            'name'        => 'Posts',
            'description' => '',

            'package_name' => 'softici/posts-module',
            'git'          => 'https://gitlab.com/softici/admincore/posts-module',
        ],
        [
            'name'        => 'Item-tags',
            'description' => '',

            'package_name' => 'softici/item-tags-module',
            'git'          => 'https://gitlab.com/softici/admincore/item-tags-module',
        ],
    ],
];
