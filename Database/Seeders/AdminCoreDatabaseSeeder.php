<?php

namespace Modules\AdminCore\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Users\ModelServices\UserService;

class AdminCoreDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $us = new UserService();
        $us->create([
            'name'     => 'Jan Novák',
            'email'    => 'admin@admin.com',
            'password' => 'secret',
        ]);

        echo "Example user admin@admin.com created (password: secret)\n";
    }
}
