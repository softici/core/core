<?php

namespace Modules\AdminCore\Console;

use Illuminate\Console\Command;
use Modules\AdminCore\Installer\ComposerService;

class InstallModule extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'acore:module';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $is = new ComposerService();

        $modules = config('admincore.modules');

        // Get modules from config
        $modules     = collect($modules);
        $modules_arr = $modules->sortBy('name')->pluck('name')->toArray();



        $updating = true;

        $selected_modules = collect();

        // Select first module
        $module_name = $this->choice("Choose modules you want to pull and install into admin core", $modules_arr);
        $module      = $modules->where('name', $module_name)->first();

        $selected_modules->add($module);


        array_push($modules_arr, '>> Install selected modules');

        // Select additional modules
        while ($updating)
        {
            $modules_implode = $selected_modules->implode('name', ', ');

            // Choose module
            $module_name = $this->choice("Choose module you want to pull and install into admin core. (Selected: $modules_implode)", $modules_arr);
            $module      = $modules->where('name', $module_name)->first();

            if ($module_name == '>> Install selected modules')
            {
                break;
            }

            // Add into selected modules
            $selected_modules->add($module);
        }


        // Install selected modules and exit
        foreach ($selected_modules as $module)
        {
            $module_name = $module['name'];

            // Install composer
            $this->info("Adding module $module_name GitLab repository to composer.json.");

            $is->requireRepository($module['git'], $module_name);
        }

        $this->info('Installing selected modules.');
        $is->requirePackage($selected_modules);

        $this->info('Softíci admin core modules installed successfully.');
    }
}
