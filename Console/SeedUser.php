<?php

namespace Modules\AdminCore\Console;

use Illuminate\Console\Command;

class SeedUser extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'acore:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed admin user in admin core.';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('module:seed', [
            'module' => 'AdminCore',
        ]);

        $this->info('Admin user admin@admin.com created. Password: secret');
    }
}
