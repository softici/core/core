<div class="card">
    <div class="card-header">IT podpora</div>
    <div class="card-body">
        <p>Pokud by bylo potřeba přidat nové funkce, upravit stávající nebo jen s čímkoli poradit,
            kontaktujte nás.</p>

        <p>mobil: <a href="tel:+420603550002">603 550 002</a></p>
        <p>e-mail: <a href="mailto:podpora@softici.cz">podpora@softici.cz</a></p>

        <a href="//softici.cz">
            <img src="{{Module::asset('admincore:images/logo_softici.png')}}"
                 width="200px"
                 alt="">
        </a>
    </div>
</div>
