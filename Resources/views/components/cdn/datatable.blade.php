<link rel="stylesheet"
      type="text/css"
      href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

<script type="text/javascript"
        charset="utf8"
        src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#table-quotes').DataTable({
                language: {
                    "sEmptyTable": "Tabulka neobsahuje žádná data",
                    "sInfo": "Zobrazuji _START_ až _END_ z celkem _TOTAL_ záznamů",
                    "sInfoEmpty": "Zobrazuji 0 až 0 z 0 záznamů",
                    "sInfoFiltered": "(filtrováno z celkem _MAX_ záznamů)",
                    "sInfoPostFix": "",
                    "sInfoThousands": " ",
                    "sLengthMenu": "Zobraz záznamů _MENU_",
                    "sLoadingRecords": "Načítám...",
                    "sProcessing": "Provádím...",
                    "sSearch": "Hledat:",
                    "sZeroRecords": "Žádné záznamy nebyly nalezeny",
                    "oPaginate": {
                        "sFirst": "První",
                        "sLast": "Poslední",
                        "sNext": "Další",
                        "sPrevious": "Předchozí"
                    },
                    "oAria": {
                        "sSortAscending": ": aktivujte pro řazení sloupce vzestupně",
                        "sSortDescending": ": aktivujte pro řazení sloupce sestupně"
                    }
                }
            });
        });
    </script>
@endpush
