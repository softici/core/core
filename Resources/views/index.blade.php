@extends('admincore::layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="page-header">
                <h4>Administrace webu</h4>
            </div>

            <p>Vítejte v administraci webu <a href="{{route('admincore::home')}}">{{ config('app.name') }}</a>.</p>

            @include('admincore::components.changelog')
        </div>

        <div class="col-md-3">
            @include('admincore::components.support')
        </div>
    </div><!--/.row-->

@endsection
