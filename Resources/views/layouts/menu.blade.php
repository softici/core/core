@php
    use Modules\AdminCore\Menu\MenuService;

    $menu_service = new MenuService();
    $sections = $menu_service->getMenuSections();
@endphp

@auth
    @foreach($sections as $section)
        <li class="sidebar-section">{{$section[0]->section}}</li>

        @foreach($section as $item)
            <li {!! (Request::route()->getName() == $item->route_name) ? 'class="active"' : '' !!}>
                <a href="{{ route($item->route_name) }}">{{ $item->name }}</a>
            </li>
        @endforeach
    @endforeach


@endauth
