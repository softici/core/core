<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Auth routes.
 *
 * Supports login only at this moment.
 */
Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false,
]);

/**
 * Admin routes.
 */
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

    // Admin dashboard
    Route::get('/', 'DashboardController@index')->name('admincore::home');
});
