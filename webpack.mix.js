const mix = require('laravel-mix');


mix.setPublicPath('Resources/assets');

mix.js(__dirname + '/Resources/src/js/app.js', 'js/app.js')
    .sass(__dirname + '/Resources/src/sass/app.scss', 'css/admin.css');

if (mix.inProduction()) {
    mix.version();
}
